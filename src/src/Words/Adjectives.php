<?php


namespace App\Words;


class Adjectives
{
    public const ADJECTIVE = [
        [
            'm' => 'blogietė',
            'v' => 'blogietis'
        ],
        [
            'v' => 'storuliukas',
            'm' => 'storuliuke'
        ],
        [
            'v' => 'ploniukas',
            'm' => 'plonytė'
        ],
        [
            'v' => 'aukštielninkas',
            'm' => 'aukštielninka'
        ],
        [
            'v' => 'išverstaakis',
            'm' => 'išverstaakė'
        ],
        [
            'v' => 'pūsliagalvis',
            'm' => 'pūsliagalvė'
        ],
        [
            'v' => 'ilgšius',
            'm' => 'ilgšė'
        ],
        [
            'v' => 'ilgalūpis',
            'm' => 'ilgalūpė'
        ],
        [
            'v' => 'pūsliagalvis',
            'm' => 'pūsliagalvė'
        ],
    ];
}