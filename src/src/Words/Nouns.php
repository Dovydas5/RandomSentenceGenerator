<?php


namespace App\Words;


class Nouns
{
    public const NOUNS = [
            ['v' => 'kopustas'],
            ['v' => 'erelis'],
            ['v' => 'katinas'],
            ['v' => 'šuo'],
            ['v' => 'žasinas'],
            ['v' => 'kiaulius'],
            ['v' => 'rupūžius'],

            ['m' => 'blusa'],
            ['m' => 'kiaulė'],
            ['m' => 'rupūžė'],
            ['m' => 'mažulė'],
            ['m' => 'mergelė'],
            ['m' => 'meilutė'],
            ['m' => 'katytė'],
    ];

}