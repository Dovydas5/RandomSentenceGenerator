<?php


namespace App\Words;


class Verbs
{

    public const VERBS = [
        [
            'v' => 'sliaužiojantis',
            'm' => 'sliaužiojanti'
        ],
        [
            'm' => 'bėganti',
            'v' => 'bėgantis'
        ],
        [
            'm' => 'rėkianti',
            'v' => 'rėkiantis'
        ],
        [
            'm' => 'skraidanti',
            'v' => 'skraidantis'
        ],
        [
            'm' => 'staugianti',
            'v' => 'staugiantis'
        ],
        [
            'm' => 'mykianti',
            'v' => 'mykiantis'
        ],
        [
            'm' => 'burbuliuojanti',
            'v' => 'burbuliuojantis'
        ],
        [
            'm' => 'šnibždanti',
            'v' => 'šnibždantis'
        ],
        [
            'm' => 'lojanti',
            'v' => 'lojantis'
        ],


    ];
}