<?php


namespace App\Controller;

use App\Entity\Sentence;
use App\Generator\RandomSlugGenerator;
use App\Generator\SentenceGenerator;
use App\Repository\SentenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SentenceController extends AbstractController
{
    /**
     * @Route("/", name="random_sentence")
     */
    public function show(
        SentenceGenerator $generator,
        RandomSlugGenerator $slugGenerator,
        Request $request
)
    {
        $randomSentence = $generator->generate();
        $sentence = new Sentence();
        $sentence->setSentence($randomSentence);
        $form = $this->createFormBuilder($sentence)
            ->add('sentence', HiddenType::class, ['label' => false])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Sentence $sentence */
            $sentence = $form->getData();
            $slug = $slugGenerator->getSlug();
            $sentence->setSlug($slug);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sentence);
            $entityManager->flush();
            return $this->redirectToRoute('slug_name', ['slug' => $slug]);
        }

        return $this->render('sentence/show.html.twig', [
            'randomSentence' => $randomSentence,
            'form' => $form->createView()

        ]);
    }

    /**
     * @Route("/{slug}", name="slug_name")
     */
    public function showSlug(string $slug, SentenceRepository $repository)
    {
        $sentence = $repository->findOneBy(['slug'=> $slug]);
        return $this->render('sentence/saved_sentence.html.twig', [
            'sentence' => $sentence,
        ]);
    }


}