<?php


namespace App\Generator;


use App\Words\Emoticons;

class EmoticGenerator
{

    public function getEmotic(): string
    {
        $index = array_rand(Emoticons::EMOTICON);
        $emotic = Emoticons::EMOTICON[$index];
        return $emotic;

    }
}