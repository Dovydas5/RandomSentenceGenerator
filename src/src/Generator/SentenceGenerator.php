<?php


namespace App\Generator;


use App\Words\Adjectives;
use App\Words\Emoticons;
use App\Words\Nouns;
use App\Words\Verbs;

class SentenceGenerator
{

    /**
     * @var EmoticGenerator
     */
    /**
     * @var UniqueWordGenerator
     */
    private $emoticGenerator;
    private $uniqueWordGenerator;

    public function __construct(
        EmoticGenerator $emoticGenerator,
        UniqueWordGenerator $uniqueWordGenerator
    )
    {
        $this->emoticGenerator = $emoticGenerator;
        $this->uniqueWordGenerator = $uniqueWordGenerator;
    }


    public function generate(){
        $nounIndex = array_rand(Nouns::NOUNS);
        $noun = Nouns::NOUNS[$nounIndex];
        $gender = array_key_first($noun);
        $nounWord = reset($noun);

        $adjectiveOne = $this->uniqueWordGenerator->getWord(Adjectives::ADJECTIVE, $gender);
        $adjectiveTwo = $this->uniqueWordGenerator->getWord(Adjectives::ADJECTIVE, $gender, $adjectiveOne);
        $verb = $this->uniqueWordGenerator->getWord(Verbs::VERBS, $gender);
        $emoticons = $this->emoticGenerator->getEmotic();
        return   "$emoticons tu $adjectiveOne, $verb, $adjectiveTwo, $nounWord.";
    }

}