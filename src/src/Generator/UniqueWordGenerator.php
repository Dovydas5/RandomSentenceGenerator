<?php


namespace App\Generator;


class UniqueWordGenerator
{
    public function getWord(array $words, string $gender, string $repeatedWord = '')
    {
        if(!$words){
            return '';
        }

        while(true) {
            $index = array_rand($words);
            $wordGroup = $words[$index];
//
//            $gender = 'm';
//            $wordGroup = ['v' => 'aaa'];

            $word = $wordGroup[$gender] ?? reset($wordGroup);
            if ($word !== $repeatedWord) {
                break;
            }
        }

        return $word;

    }
}