<?php

namespace App\Tests\AppBundle\Generator;

use App\Generator\EmoticGenerator;
use App\Generator\SentenceGenerator;
use App\Generator\UniqueWordGenerator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class SentenceGeneratorTest extends TestCase
{
    public function testGetSentence(){
        /** @var EmoticGenerator|MockObject $emoticGeneratorMock */
        $emoticGeneratorMock = $this->createMock(EmoticGenerator::class);
        $emoticGeneratorMock
            ->expects($this->once())
            ->method('getEmotic')
            ->will($this->returnValue('vaivaivai'));
        /** @var UniqueWordGenerator|MockObject $uniqueWordGenerator */
        $uniqueWordGenerator = $this->createMock(UniqueWordGenerator::class);
        $uniqueWordGenerator
            ->expects($this->exactly(3))
            ->method('getWord')
            ->willReturn('taspats');
        $generator = new SentenceGenerator($emoticGeneratorMock,$uniqueWordGenerator);
        $result = $generator->generate();
        $this->assertStringContainsString('vaivaivai tu taspats, taspats, taspats, ',$result);
    }

    public function testSuccessSentence()
    {
        $generator = new SentenceGenerator(
            new EmoticGenerator(),
            new UniqueWordGenerator()
        );

        $result = $generator->generate();
        $this->assertEquals('.', substr($result, -1));
        $this->assertEquals(3, substr_count($result ,',' ));
        $this->assertEquals(7,str_word_count($result));
    }
}
