<?php

namespace App\Tests\AppBundle\Generator;

use App\Generator\UniqueWordGenerator;
use PHPUnit\Framework\TestCase;

class UniqueWordGeneratorTest extends TestCase
{


    /**
     * @dataProvider wordsDataProvider
     */
    public function testGetWord(
        array $words,
        string $gender,
        string $repeatedWord,
        string $expected)
    {
        $generator = new uniqueWordGenerator();
        $result = $generator->getWord($words, $gender, $repeatedWord);
        $this->assertEquals($expected, $result);
    }

    public function wordsDataProvider()
    {
        return [
            [
                [
                    [
                        'm' => 'am',
                        'v' => 'av'
                    ],
                    [
                        'v' => 'bv',
                        'm' => 'bm'
                    ]
                ],
                'v',
                'bv',
                'av'
            ],
            [
                [
                    [
                        'm' => 'am',
                        'v' => 'av'
                    ],
                ],
                'v',
                '',
                'av'
            ],
            [
                [
                    [
                        'v' => 'av'
                    ],
                ],
                'm',
                '',
                'av'
            ],
            [
                [],
                'm',
                '',
                ''
            ],
            [
                [[]],
                '',
                '',
                ''
            ],
        ];
    }
}
