<?php

namespace App\Tests\AppBundle\Generator;

use App\Generator\EmoticGenerator;
use App\Words\Emoticons;
use PHPUnit\Framework\TestCase;

class EmoticGeneratorTest extends TestCase
{
       public function testGetEmotic(): void
       {
           $generator = new EmoticGenerator();
           $result = $generator->getEmotic();
           $arrayHasResult = in_array($result,Emoticons::EMOTICON);

           $this->assertTrue($arrayHasResult);

       }
}
