<?php


namespace App\Tests\AppBundle\Generator;


use App\Generator\RandomSlugGenerator;
use PHPUnit\Framework\TestCase;

class SlugTest extends TestCase
{
    public function testSlug(){

        $slug = new RandomSlugGenerator();
        $this->assertIsNotString([$slug->getSlug()]);


    }

}