<?php


namespace App\Tests\AppBundle\Controller;

use App\Entity\Sentence;
use App\Generator\SentenceGenerator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SentenceControllerTest extends WebTestCase
{
    public function testShowPost()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseStatusCodeSame(200, $client->getResponse()->getStatusCode());

        $sentence = $crawler->filter('#sentence')->first()->text();
        $words = explode(' ',$sentence);

        $this->assertStringContainsString(' tu ', $sentence);
        $this->assertCount(6, $words);

    }
    public function testSentenceDuplication()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $firstSentenceElement = $crawler->filter('#sentence');
        $this->assertEquals(1, $firstSentenceElement->count());
        $fistSentence = $firstSentenceElement->first()->text();
        // Second Sentence

        $crawler = $client->request('GET', '/');
        $secondSentenceElement = $crawler->filter('#sentence');
        $this->assertEquals(1, $secondSentenceElement->count());
        $secondSentence = $secondSentenceElement->first()->text();

        $this->assertNotEquals($fistSentence, $secondSentence);



    }
}